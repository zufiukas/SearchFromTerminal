use std::env;
use webbrowser;

const VALID_WEBSITES: [&str; 4] = [
    "reddit.com",
    "stackoverflow.com",
    "stackexchange.com",
    "medium.com",
];

fn create_filter(valid_websites: &[&str]) -> String {
    let mut filter = String::from("(");
    for (index, website) in valid_websites.iter().enumerate() {
        filter += &format!("site:{}", website);
        if index == valid_websites.len() - 1 {
            filter += ")";
        } else {
            filter += " OR ";
        }
    }
    filter
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let sliced = &args[1..];
    let query = sliced.join("+");

    if query.is_empty() {
        eprintln!("No search query provided. Please provide a search query.");
        main();
        return;
    }

    let filter = create_filter(&VALID_WEBSITES);
    let url = format!("https://www.google.com/search?q={}+{}", filter, query);
    webbrowser::open(&url).unwrap();
}

